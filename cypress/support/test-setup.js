import { setupCypressInterception } from './utils/auto-stub';

beforeEach(() => {
  setupCypressInterception();
});
